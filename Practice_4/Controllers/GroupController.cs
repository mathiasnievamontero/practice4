﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Practice4.Logic.Models;
using Practice4.Logic.Managers;

namespace Practice_4.Controllers
{
    [ApiController]
    [Route("/api/groups")]
    public class GroupController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly GroupManager _groupManager;
        public GroupController(IConfiguration config)
        {
            _config = config;
            _groupManager = new GroupManager();
        }

        [HttpGet]
        public List<Group> GetGroups()
        {
            return _groupManager.GetAllGroups();
        }
        
        [HttpPost]
        public Group CreateGroup([FromBody]string groupName, int availableSlots)
        {
            return _groupManager.CreateGroup(groupName, availableSlots);
        }

        [HttpPut]
        public Group UpdateGroup([FromBody]Group group)
        {
            return _groupManager.UpdateGroup(group);
        }

        [HttpDelete]
        public Group DeleteGroup([FromBody]Group group)
        {
            return _groupManager.DeleteGroup(group);
        }
    }
}
