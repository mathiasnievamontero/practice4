using System;
using System.Collections.Generic;
using Practice4.Data.Models;


namespace Practice4.Data
{
    public class DbContext
    {
        // Acces to DB server

        public List<Group> GroupTable { get; set; }
        public DbContext()
        {
            // read from config file the DB Connection String
            GroupTable = new List<Group>()
            {
                new Group() { Name = "Jose", Id = 1, AvailableSlots = 5 },
                new Group() { Name = "Pepe", Id = 2, AvailableSlots = 12 },
                new Group() { Name = "Carlos", Id = 3, AvailableSlots = 3 }
            };
        }

        public Group AddGroup(Group group)
        {
            GroupTable.Add(group);
            return group;
        }

        public Group UpdateGroup(Group groupToUpdate)
        {
            Group foundGroup = GroupTable.Find(group => group.Id == groupToUpdate.Id);
            foundGroup.Name = groupToUpdate.Name;
            foundGroup.AvailableSlots = groupToUpdate.AvailableSlots;
            
            return foundGroup;
        }

        public Group DeleteGroup(Group groupToDelete)
        {
            GroupTable.Remove(groupToDelete);
            return groupToDelete;
        }

        public List<Group> GetAll()
        {
            return GroupTable;
        }
    }
}
