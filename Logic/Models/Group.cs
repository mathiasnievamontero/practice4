using System;

namespace Practice4.Logic.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
    }
}
