using System;
using System.Collections.Generic;
using Practice4.Data;

namespace Practice4.Logic.Models
{
    public static class DTOMappers
    {
        public static List<Group> MapGroups(List<Data.Models.Group> groups)
        {
            List<Group> mappedGroup = new List<Group>();

            foreach (Data.Models.Group group in groups)
            {
                mappedGroup.Add(new Group()
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });
            }

            return mappedGroup;
        }

        public static Group MapGroup(Data.Models.Group group)
        {
            return new Group() { Name = group.Name, AvailableSlots = group.AvailableSlots, Id = group.Id };
        }
    }
}
