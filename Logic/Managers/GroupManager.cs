using System;
using System.Collections.Generic;
using Practice4.Logic.Models;
using Practice4.Data;
using System.Linq;


namespace Practice4.Logic.Managers
{
    public class GroupManager
    {
        private readonly DbContext _dbContext;

        public GroupManager()
        {
            _dbContext = new DbContext();
        }

        public List<Group> GetAllGroups()
        {
            /*
            string projectTitle = _config.GetSection("Project").GetSection("Ttile").Value;
            string dbConnection = _config.GetConnectionString("Database");
            Console.WriteLine($"We are connecting to: {dbConnection}");
            */
            List<Data.Models.Group> groups = _dbContext.GetAll();
            return DTOMappers.MapGroups(groups);
        }

        public Group CreateGroup(string groupName, int availableSlots)
        {
            var grouplist = GetAllGroups();
            int id = 1;
            if (grouplist.Count > 0)
                id = grouplist.Last().Id + 1;

            if (availableSlots < 1)
            {
                availableSlots = 1;
                Console.Out.WriteLine("must have at least 1 slot");
            }              
            
            if (groupName.Length > 50)
            {
                groupName = groupName.Remove(50);
                Console.Out.WriteLine("No more than 50 caracters");
            }

            if (groupName.Length < 1)
            {
                groupName = "???";
                Console.Out.WriteLine("Cant have empty name");
            }              

            Data.Models.Group group = new Data.Models.Group() { Name = groupName, AvailableSlots = availableSlots, Id = id };

            _dbContext.AddGroup(group);

            return DTOMappers.MapGroup(group); 
        }

        public Group UpdateGroup(Group group)
        {
            if (group.AvailableSlots < 1)
            {
                group.AvailableSlots = 1;
                Console.Out.WriteLine("must have at least 1 slot");
            }
            
            if (group.Name.Length > 50)
            {
                group.Name = group.Name.Remove(50);
                Console.Out.WriteLine("No more than 50 caracters");
            }

            if (group.Name.Length < 1)
            {
                group.Name = "???";
                Console.Out.WriteLine("Cant have empty name");
            }

            _dbContext.UpdateGroup(new Data.Models.Group() { Name = group.Name, Id = group.Id, AvailableSlots = group.AvailableSlots});
            return group;
        }

        public Group DeleteGroup(Group group)
        {
            _dbContext.DeleteGroup(new Data.Models.Group() { Name = group.Name, Id = group.Id, AvailableSlots = group.AvailableSlots});
            return group;
        }
    }
}
